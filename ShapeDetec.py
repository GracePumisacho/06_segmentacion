import sys
import numpy as np
import cv2


image_path1 = "./imagenes/img.jfif"
image_path2 = "./imagenes/img.jfif"

def get_ref_contour(img):
 ref_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
 ret, thresh = cv2.threshold(ref_gray, 127, 255, 0)

 contours, hierarchy = cv2.findContours(thresh, 1, 2)

 for contour in contours:
     area = cv2.contourArea(contour)
     img_area = img.shape[0] * img.shape[1]
     if 0.05 < area/float(img_area) < 0.8:
         return contour
# Extract all the contours from the image
def get_all_contours(img):
    ref_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(ref_gray, 127, 255, 0)
    contours, hierarchy = cv2.findContours(thresh, 1, 2)
    return contours

if __name__=='__main__':
    img1 = cv2.imread(image_path2)
    img2 = cv2.imread(image_path1)
    ref_contour = get_ref_contour(img1)
    input_contours = get_all_contours(img2)
    closest_contour = input_contours[0]
    min_dist = sys.maxsize
    for contour in input_contours:
        ret = cv2.matchShapes(ref_contour, contour, 1, 0.0)
        if ret < min_dist:
            min_dist = ret
            closest_contour = contour
    cv2.drawContours(img2, [closest_contour], -1, (0, 0, 0), 3)
    cv2.imshow('Output', img2)
    cv2.waitKey()