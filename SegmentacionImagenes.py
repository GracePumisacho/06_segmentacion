import cv2
import numpy as np

image =cv2.imread('./imagenes/img.jfif')

gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

edged= cv2.Canny(gray, 50, 200)
cv2.imshow('1- Canny Edges', edged)
cv2.waitKey(0)

contours, hierarchy = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
print ("Number of contours found= ", len(contours))

blank_image = np.zeros(image.shape, np.uint8)
cv2.drawContours(blank_image, contours, -1, (0,255,0), 3)
cv2.imshow('2 -All Contours over blank image', blank_image)
cv2.waitKey(0)


